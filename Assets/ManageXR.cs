using System.Collections;
using System.Collections.Generic;
using MXR.SDK;
using UnityEngine;

public class ManageXR : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MXRManager.Init();

        string serialNumber = MXRManager.System.DeviceStatus.serial;
        Debug.Log("Serial Number: " + serialNumber);
    }
}
